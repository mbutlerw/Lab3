package ie.ucd.luggage;

public class Laptop implements Item{
	private double weight;
	private String type;
	private boolean danger;
	
	public Laptop() {
		this.weight = 2400;
		this.type = "Laptop";
		this.danger = false;
	}

	@Override
	public String getType() {
		return this.type;
	}

	@Override
	public double getWeight() {
		return this.weight;
	}

	@Override
	public boolean isDangerous() {
		return this.danger;
	}

}
