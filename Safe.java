package ie.ucd.luggage;

public class Safe extends Luggage {
	private String password;
	private double weight;
	private double Maxweight;

	public Safe(String password, double weight) {
		this.password = password;
		this.weight = weight;
		this.Maxweight = 22000;
	}

	@Override
	public double getBagWeight() {
		return this.weight;
	}

	@Override
	public double getMaxWeight() {
		return this.Maxweight;
	}
	
	private boolean passcheck(String pass) {
		//Password checking system
		if(pass == this.password) {
			System.out.println("Password accepted");
			return true;
		}
		else {
			System.out.println("Password incorrect");
			return false;
		}
	}
	
	//Function to update the password
	public void passupdate(String passn) {
		/* Prompt user for password
		 * This may have issues running from some IDEs
		 * but it is still functional Java code when run 
		 * from a command line
		 */
		java.io.Console console = System.console();
		String pass = new String(console.readPassword("Password: "));
		if (passcheck(pass)) {
			this.password = passn;
			System.out.println("Password updated");
		}
	}

	public void addItem(Item i) {
		//Prompt user for password
		java.io.Console console = System.console();
        String pass = new String(console.readPassword("Password: "));
		if(passcheck(pass)) {
			//Add item
			this.add(i);
		}
		else {
			//Do nothing
		}
	}
	
	public void removeItem(int i) {
		//Prompt user for password
		java.io.Console console = System.console();
        String pass = new String(console.readPassword("Password: "));
		if(passcheck(pass)) {
			//Remove item
			this.removeItem(i);;
		}
		else {
			//Do nothing
		}
	}
}
