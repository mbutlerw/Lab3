package ie.ucd.luggage;

public class Pen implements Item{
	private double weight;
	private String type;
	private boolean danger;

	public Pen() {
		this.weight = 5.8;
		this.type = "Pen";
		this.danger = false;
	}

	@Override
	public String getType() {
		return this.type;
	}

	@Override
	public double getWeight() {
		return this.weight;
	}

	@Override
	public boolean isDangerous() {
		return this.danger;
	}

}
