package ie.ucd.luggage;

public class DesignerPen extends Pen {
	private String brand;

	public DesignerPen(String bran) {
		this.brand = bran;
	}
	
	@Override
	public String getType() {
		return this.brand+" Pen";
	}
}