package ie.ucd.luggage;

public class Suitcase extends Luggage {
	private double bagWeight;
	private double maxWeight;
	
	public Suitcase(double weight) {
		this.bagWeight = weight;
		this.maxWeight = 24000;
	}

	@Override
	public double getBagWeight() {
		return this.bagWeight;
	}

	@Override
	public double getMaxWeight() {
		return this.maxWeight;
	}

	public String getType() {
		return "Suitcase";
	}
}
