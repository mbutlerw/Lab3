package ie.ucd.luggage;

public class Bomb implements Item {
	private double Weight;
	private String type;
	private boolean danger;

	public Bomb() {
		this.danger = true;
		this.type = "Bomb";
		this.Weight = 190;
	}

	@Override
	public String getType() {
		return this.type;
	}

	@Override
	public double getWeight() {
		return this.Weight;
	}

	@Override
	public boolean isDangerous() {
		return this.danger;
	}

}
